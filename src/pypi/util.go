// Author: ymotongpoo (Yoshifumi YAMAGUCHI)

package pypi

import (
	"os"
	"os/exec"
)

func Unzip(f *os.File) error {
	cmd := exec.Command("unzip", f.Name())
	err := cmd.Run()
	return err
}

func Untargz(f *os.File) error {
	cmd := exec.Command("tar", "xzf", f.Name())
	err := cmd.Run()
	return err
}
