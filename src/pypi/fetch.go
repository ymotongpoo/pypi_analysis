// Author: ymotongpoo (Yoshifumi YAMAGUCHI)

package pypi

import (
	"encoding/json"
	"errors"
	"fmt"
	"github.com/mattn/go-xmlrpc"
	"io/ioutil"
	"net/http"
	"os"
	"os/exec"
	"path"
	"strings"
	"time"
)

type PackageInfo struct {
	Name            string
	Version         string
	StableVersion   *string `json:"stable_version"`
	Author          *string
	AuthorEmail     *string `json:"author_email"`
	Maintainer      *string
	MaintainerEmail *string `json:"maintainer_email"`
	Homepage        *string `json:"info.home_page"`
	License         *string
	Summary         *string
	Description     *string
	Platform        *string
	Classifires     []*string
}

type PackageUrls struct {
	Url           string
	LastUpdateStr string `json:"upload_time"`
	LastUpdate    time.Time
	Downloads     int
	PythonVer     string `json:"python_version"`
	Type          string `json:"packagetype"`
}

type Package struct {
	Info PackageInfo
	Urls []PackageUrls
}

type Dependency struct {
	Package   string
	Downloads int
	DepPkg    string
}

const serverProxy = "http://pypi.python.org/pypi/"

// Source filename
func (p Package) SourceUrl() string {
	for _, u := range p.Urls {
		if u.Type == "sdist" {
			return u.Url
		}
	}
	return ""
}

func (p Package) TotalDownloads() int {
	var total = 0
	for _, u := range p.Urls {
		total += u.Downloads
	}
	return total
}

func (p Package) fileDirNames() (string, string) {
	url := p.SourceUrl()
	filename := path.Base(url)
	dirname := strings.Replace(filename, ".zip", "", -1)
	dirname = strings.Replace(dirname, ".tar.gz", "", -1)
	return filename, dirname
}

// Search setup.py or setup.cfg
func (p Package) PkgDependencies() ([]string, error) {
	pwd, err := os.Getwd()
	if err != nil {
		return []string{}, err
	}

	_, dirname := p.fileDirNames()

	// TODO(ymotongpoo): check requires.txt to check required packages
	//   $ python setup.py egg_info
	//   $ cat <pkg_name>.egg_info/requirements.txt
	// In the case of distutils, requirements.txt will not be created, then
	// --requiers option might show required packages (need confirmation)
	//   $ python setup.py --requires

	// 1. Run egg_info with assumption that setup.py uses 'setuptools'
	err = os.Chdir(path.Join(pwd, dirname))
	if err != nil {
		return []string{}, err
	}

	cmd := exec.Command("python", "setup.py", "egg_info")
	err = cmd.Run()
	if err != nil {
		fmt.Errorf("cannot run egg_info not found")
		// 2. Run --requires option
		// TODO(ymotongpoo): requires stdout dump of executed result
		cmd = exec.Command("python", "setup.py", "--requires")
		err = cmd.Run()
		if err != nil {
			_ = os.Chdir(pwd)
			return []string{}, err
		}
	}
	egg_info := p.Info.Name + ".egg-info"
	requirements_file := path.Join(egg_info, "requires.txt")
	buf, err := ioutil.ReadFile(requirements_file)
	if err != nil {
		buf, err = ioutil.ReadFile(path.Join("src", requirements_file))
	}
	if err != nil {
		_ = os.Chdir(pwd)
		return []string{}, err
	}

	err = os.Chdir(pwd)
	if err != nil {
		return []string{}, err
	}

	lines := strings.Split(string(buf), "\n")
	var deps []string
	for _, l := range lines {
		if len(l) > 0 && string(l[0]) != `[` {
			deps = append(deps, l)
		}
	}

	return deps, nil
}

// Remove package source archive and expanded directory
func (p Package) RemoveSource() error {
	filename, dirname := p.fileDirNames()
	err := os.Remove(filename)
	if err != nil {
		return err
	}

	err = os.RemoveAll(dirname)
	if err != nil {
		return err
	}
	return nil
}

// Fetch all package list from PyPI XML-RPC
func FetchPkgListData() ([]string, error) {
	resp, err := xmlrpc.Call(serverProxy, "list_packages")
	if err != nil {
		return nil, err
	}
	packages := make([]string, len(resp.(xmlrpc.Array)))
	for i, p := range resp.(xmlrpc.Array) {
		packages[i] = p.(string)
	}

	return packages, nil
}

// Fetch package data via PyPI JSON API
func FetchPkgData(name string) (Package, error) {
	resp, err := http.Get(serverProxy + name + "/json")
	if err != nil && resp.StatusCode == 404 {
		return Package{}, err
	}

	data, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return Package{}, err
	}

	var p Package
	err = json.Unmarshal(data, &p)
	if err != nil {
		return Package{}, err
	}

	for i, u := range p.Urls {
		p.Urls[i].LastUpdate, err = time.Parse("2006-01-02T15:04:05", u.LastUpdateStr)
		if err != nil {
			return Package{}, err
		}
	}

	return p, nil
}

// Fetch package tarball/zip file
func FetchPkgSource(name string) (*os.File, error) {
	p, err := FetchPkgData(name)
	if err != nil {
		return nil, err
	}

	url := p.SourceUrl()
	if url == "" {
		return nil, errors.New("FetchPkgSource: no file")
	}

	filename := path.Base(url)
	file, err := os.Create(filename)
	if err != nil {
		return nil, err
	}

	resp, err := http.Get(url)
	if err != nil {
		return nil, err
	}

	data, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return nil, err
	}

	_, err = file.Write(data)
	if err != nil {
		return nil, err
	}

	return file, nil
}
