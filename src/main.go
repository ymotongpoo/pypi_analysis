// Author: ymotongpoo (Yoshifumi YAMAGUCHI)

package main

import (
	"flag"
	"fmt"
	"log"
	"os"
	"path"
	"pypi"
	"strings"
	"time"
)

func extractPkgName(dep string) string {
	dep = strings.Replace(dep, ">", " ", -1)
	dep = strings.Replace(dep, "=", " ", -1)
	dep = strings.Replace(dep, "<", " ", -1)
	dep = strings.Trim(dep, " ")
	return strings.Split(dep, " ")[0]
}

func pkg(name string) ([]pypi.Dependency, error) {
	p, err := pypi.FetchPkgData(name)
	if err != nil {
		return nil, err
	}
	file, err := pypi.FetchPkgSource(p.Info.Name)
	if err != nil {
		return nil, err
	}
	ext := path.Ext(file.Name())
	if ext == ".zip" {
		err = pypi.Unzip(file)
		if err != nil {
			return nil, err
		}
	} else if ext == ".gz" {
		err = pypi.Untargz(file)
		if err != nil {
			return nil, err
		}
	}

	deps, err := p.PkgDependencies()
	if err != nil {
		_ = p.RemoveSource()
	}

	var result []pypi.Dependency
	if len(deps) > 0 {
		result = make([]pypi.Dependency, len(deps))
		for i, d := range deps {
			result[i] = pypi.Dependency{
				Package:   p.Info.Name,
				Downloads: p.TotalDownloads(),
				DepPkg:    extractPkgName(d),
			}
		}
	} else {
		result = []pypi.Dependency{
			pypi.Dependency{
				Package:   p.Info.Name,
				Downloads: p.TotalDownloads(),
				DepPkg:    "",
			},
		}
	}

	err = p.RemoveSource()
	if err != nil {
		return nil, err
	}

	return result, nil
}

func dump(index string) error {
	all_packages, err := pypi.FetchPkgListData()
	if err != nil {
		return err
	}

	var packages []string
	for _, p := range all_packages {
		if strings.ToLower(p)[0] == uint8(index[0]) {
			packages = append(packages, p)
		}
	}

	file, _ := os.Create(index + ".csv")
	defer file.Close()

	for _, p := range packages {
		deps, err := pkg(p)
		if err != nil {
			log.Printf("%v: %v\n", p, err)
			time.Sleep(5 * time.Second)
			continue
		}
		for _, d := range deps {
			_, _ = fmt.Fprintf(file, "%s,%s,%d\n", 
				d.Package, d.DepPkg, d.Downloads)
		}
		time.Sleep(10 * time.Second)
	}
	return nil
}

var All bool
var Pkg string
var Dump string

func init() {
	flag.BoolVar(&All, "all", false, "get all packages list")
	flag.StringVar(&Pkg, "pkg", "", "get specified package information")
	flag.StringVar(&Dump, "dump", "", "dump package information")
}

func main() {
	flag.Parse()
	if All && (Pkg != "") {
		fmt.Println("cannot specify both flags")
	}

	if All {
		packages, err := pypi.FetchPkgListData()
		if err != nil {
			log.Fatalf("error, %s", err)
		}
		for _, p := range packages {
			fmt.Println(p)
		}
	} else if Pkg != "" {
		deps, err := pkg(Pkg)
		if err != nil {
			log.Fatalf("error, %s", err)
		}
		for _, d := range deps {
			fmt.Printf("%v\n", d)
		}
	} else if Dump != "" {
		if len(Dump) != 1 {
			fmt.Printf("assign only one char for index, %v assigned\n", Dump)
		}
		err := dump(Dump)
		if err != nil {
			log.Fatalf("error, %s", err)
		}
	} else {
		fmt.Println("no package")
	}
}
